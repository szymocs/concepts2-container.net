﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PK.Container
{
    class Container:IContainer
    {
        private IDictionary<Type, Type> impelmentacjatyp;
        private IDictionary<Type, object> implementacjaobjekt;
        private IDictionary<Type, Func<object>> implementacjafunkcja;
        public Container()
        {
            impelmentacjatyp = new Dictionary<Type, Type>();
            implementacjaobjekt = new Dictionary<Type, object>();
            implementacjafunkcja = new Dictionary<Type, Func<object>>();
        }
        void IContainer.Register(System.Reflection.Assembly assembly)
        {
            if (assembly != null)
            {

                foreach (Type i in assembly.GetTypes())
                {
                    if (i.IsClass)
                    {
                        foreach (var j in i.GetType().GetInterfaces())
                        {
                            impelmentacjatyp[i] = j.GetType();
                        }

                    }
                }
            }

        }

        void IContainer.Register(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                impelmentacjatyp[i.GetType()] = type;
            }
        }

        void IContainer.Register<T>(T impl)
        {
            implementacjaobjekt[typeof(T).GetType()] = impl;
            //foreach (var i in typeof(T).GetInterfaces())
            //{
            //    implementacjaobjekt[typeof(T).GetType()]=i.GetType;
            //}
        }

        void IContainer.Register<T>(Func<T> provider)
        {
            implementacjafunkcja[typeof(T).GetType()] = provider;
        }

        T IContainer.Resolve<T>()
        {
            if (impelmentacjatyp[typeof(T).GetType()] != null)
            {
                return (T)Activator.CreateInstance((impelmentacjatyp[typeof(T).GetType()]));
            }
            if (implementacjaobjekt[typeof(T).GetType()] != null)
            {
                return (T)implementacjaobjekt[typeof(T).GetType()];
            }
            if (implementacjafunkcja[typeof(T).GetType()] != null)
            {
                return (T)implementacjafunkcja[typeof(T).GetType()]();
            }
            throw new PK.Container.UnresolvedDependenciesException("Wyjątek Resolve<T>");
        }

        object IContainer.Resolve(Type type)
        {
            if ((object)Activator.CreateInstance(impelmentacjatyp[type.GetType()]) != null)
            {
                return (object)Activator.CreateInstance(impelmentacjatyp[type.GetType()]);
            }
            else
            {
                throw new PK.Container.UnresolvedDependenciesException("Wyjątek Resolve(Type type)");
            }
        }
    }
}
